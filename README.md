# Reddit Chrome Extension - SHOW FULL IMAGE

Made an extension that automatically expands an image on reddit.
Sometimes images are cut off and you have to click, 'Show Full Image' 
And then that only opens the image to the reddit post page
So you have to click again just to view the full image.

Very annoying

Now no clicking is needed

# To install

- Navigate to chrome://extensions/
- Enable developer mode
- Load unpacked
- Select the project folder

You can check the script is working because it will show console log messages while scrolling through the reddit page  
