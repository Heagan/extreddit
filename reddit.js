let fireTime = 1000;
let lastFired = new Date().getTime()
let fixedElements = []

document.addEventListener("scroll", function() {
    let now = new Date().getTime()
    if (now - lastFired > fireTime) {
        console.log('Event fired')
        lastFired = now
        elements = document.querySelectorAll('[data-adclicklocation="title"]')
        for (ele of elements) {
            if (ele.getBoundingClientRect().x > 0) {
                if (ele.parentElement.innerText.includes('SEE FULL IMAGE')) {
                    post = ele.parentElement.querySelectorAll('[alt="Post image"]')[0]
                    if (post) {
                        clone = post.cloneNode(true)
                        ele.appendChild(clone)
                        post.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.remove()
                        fixedElements.push(ele)
                        console.log("fixed image")
                    } else {
                        if (ele.parentElement.querySelectorAll('[title="Next"]').length > 0) {
                            for (post of ele.parentElement.querySelectorAll('[alt]')) {
                                clone = post.cloneNode(true)
                                ele.appendChild(clone)
                                fixedElements.push(ele)
                                console.log("fixed image")
                            }
                            post.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.remove()
                        }
                    }
                }
            }
        }
    }
});

console.log('Reddit Script Loaded')
